;;; server.scm --- Simple HTTP server
;;; Copyright (c) 2022, DEADBLACKCLOVER.
;;;
;;; This program is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Code:
(use-modules (ice-9 rdelim) 
             (ice-9 textual-ports) 
             (web request) 
             (web server) 
             (web uri))

(define (get-content filename) 
  (call-with-input-file (string-append "." filename) 
    (lambda (port) 
      (get-string-all port))))

(define (request-path-components request) 
  (uri-path (request-uri request)))

(define (my-handler request request-body) 
  (values '((content-type . (text/plain))) 
          (get-content (request-path-components request))))

(run-server my-handler 'http '(#:port 8081))
;;; server.scm ends here
